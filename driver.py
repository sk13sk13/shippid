from reloadable import Reloadable

def clamp(a, low, high):
    """ Clamp a number between a range. """
    if a > high:
        return high
    elif a < low:
        return low
    else:
        return a   

class Driver(Reloadable):
    def __init__(self, state=None, ship=None):

        self.ship = ship
        self.reset = True

        self.last_x = ship.x
        self.i = 0

        super().__init__(state)

    def tick(self, ct):
        inputs = self.get_inputs(ct)

        if inputs:
            inputs["enabled"] = True

        if self.reset:
            inputs["reset"] = True
            self.reset = False

        return inputs

    def get_inputs(self, ct):
        factor=1
        p=0
        d=0
        i=0
        kd=1
        kp=1
        ki=1
#
# Hints for your changes here
#       You have the following variables at hand:
#       self.ship.x: position of the ship, which is 0, if the ship is at the right position (so self.ship.x can also be seen as the Regeldifferenz)
#       self.last_x: a variable, which can be used to store the previous Regeldifferenz
#       self.i: a variable, which can be used to sum up the integral part
#The following variables need to be specified/calculated
#       kp: individual factor for the proportional part
#       kd: individual factor for the derivative part
#       ki: individual factor for the intrgral part
#       p:  the value for the proportional part
#       i:  the value for the integral part
#       d:  the value for the derivative part
#
#Start you changes here

#End of your changes
        return {
            "factor": factor,
            "p": p*kp,
            "d": d*kd,
            "i": i*ki,
        }


