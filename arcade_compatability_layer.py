import arcade

def createShapeElementList():
	if(hasattr(arcade,"shape_list")):
		return arcade.shape_list.ShapeElementList()
	else:
		return arcade.ShapeElementList()

def createLine(a,b,c,d,e,f):
	if(hasattr(arcade,"shape_list")):
		return arcade.shape_list.create_line(a,b,c,d,e,f)
	else:
		return arcade.create_line(a,b,c,d,e,f)

def createRectangle(a,b,c,d,e):
	if(hasattr(arcade,"shape_list")):
		return arcade.shape_list.create_rectangle(a,b,c,d,e)
	else:
		return arcade.create_rectangle(a,b,c,d,e)

def getShipRotationAngle():
	if(hasattr(arcade,"shape_list")):
		return 90
	else:
		return -90

