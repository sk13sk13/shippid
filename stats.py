import arcade_compatability_layer
from size import real_height, INIT_SCREEN_WIDTH

import arcade

from reloadable import Reloadable


class Drawable:

    X = 0
    Y = 0
    WIDTH = INIT_SCREEN_WIDTH

    # edited on resize
    REAL_WIDTH = WIDTH

    def __init__(self, X, Y):
        self.drawables = []
        self.X = X
        self.Y = Y

    def create_line(self, start_x, start_y, end_x, end_y, color, thickness):
        self.drawables.append(
            arcade_compatability_layer.createLine(
                start_x + self.X, start_y + self.Y,
                end_x + self.X, end_y + self.Y,
                color, thickness
            )
        )

    def draw_line(self, start_x, start_y, end_x, end_y, color, thickness):
        arcade.draw_line(
            start_x + self.X, start_y + self.Y,
            end_x + self.X, end_y + self.Y,
            color, thickness
        )

    def draw(self):
        for d in self.drawables:
            d.draw()

    def resize(self, screen_width: int):
        self.REAL_WIDTH = screen_width


class Diagram(Drawable):

    HEIGHT = 180
    LINE_COLOR = (0, 255, 255)
    PADDING = 15
    LEFT_WIDTH = 180
    TEXT_SIZE = 80

    # edited on resize
    REAL_HEIGHT = HEIGHT

    def __init__(self, X, Y, label, line_color=None, value=0):
        super().__init__(X, Y)

        if line_color is not None:
            self.LINE_COLOR = line_color

        self.min_val = value-0.01
        self.max_val = value+0.01
        self.point_count = 100

        self.points = [
            self.f(value) for x in range(self.point_count)
        ]
        self.values = [
            value for x in range(self.point_count)
        ]
        self.carriers = [
            self.carrier(i) for i in range(self.point_count)
        ]

        self.label = label
        self.value=value
        self.valueLabelY=self.Y
        self.valueLabelFontSize=self.TEXT_SIZE

        self.create_lines()
    
    def create_lines(self):
        self.create_line(0, 0, self.REAL_WIDTH, 0, self.LINE_COLOR, 1)
        self.create_line(0, self.HEIGHT, self.REAL_WIDTH, self.HEIGHT, self.LINE_COLOR, 1)
        self.create_line(0, 0, 0, self.HEIGHT, self.LINE_COLOR, 1)
        self.create_line(self.REAL_WIDTH, 0, self.REAL_WIDTH, self.HEIGHT, self.LINE_COLOR, 1)
        self.create_line(self.LEFT_WIDTH, 0, self.LEFT_WIDTH, self.HEIGHT, self.LINE_COLOR, 1)

        self.create_line(
            self.carrier(0) - self.X, self.f(0) - self.Y, self.carrier(100) - self.X, self.f(0) - self.Y, (50, 50, 50), 1
        )


    def resize(self, screen_height, screen_width):
        super().resize(screen_width)
        #adjust carriers (aka x-values)
        self.carriers = [ self.carrier(i) for i in range(self.point_count) ]

        # reset drawables
        self.drawables = []
        self.REAL_HEIGHT = real_height(self.HEIGHT, screen_height)
        # recreate drawables
        self.create_lines()

    def setValueLabelY(self,y):
        self.valueLabelY=y
    
    def setValueLabelFontSize(self,s):
        self.valueLabelFontSize=s

    def f(self, v):
        h = self.REAL_HEIGHT - 2 * self.PADDING
        return self.PADDING + h / (self.max_val - self.min_val) * (v - self.min_val) + self.Y

    # off by one, but meh!
    def carrier(self, i):
        w = self.REAL_WIDTH - self.LEFT_WIDTH - 2 * self.PADDING
        return self.LEFT_WIDTH + self.PADDING + w / self.point_count * i + self.X

    def add_point(self, value):
        """Add another data point."""

        self.values.pop(0)
        self.values.append(value)
        #scale all points so that the curve fits into the display
        r=max(abs(min(self.values)),max(self.values))
        if(r==0):
            self.min_val=-0.01
            self.max_val=0.01
        else:    
            self.min_val=-r
            self.max_val=r

        self.points=[self.f(i) for i in self.values]
        self.value=value

    def draw(self):
        super().draw()

        arcade.draw_text(
            self.label,
            self.LEFT_WIDTH / 2 + self.X, self.REAL_HEIGHT / 2 + self.Y,
            self.LINE_COLOR, self.TEXT_SIZE,
            width=self.LEFT_WIDTH, align="center",
            anchor_x="center", anchor_y="center")
        
        arcade.draw_text(
            round(self.value,2),
            self.REAL_WIDTH + self.X-self.LEFT_WIDTH, self.REAL_HEIGHT / 2 + self.valueLabelY,
            self.LINE_COLOR, self.valueLabelFontSize,
            width=self.LEFT_WIDTH, align="center",
            anchor_x="center", anchor_y="center")

        points = list(zip(self.carriers, self.points))
        arcade.draw_line_strip(points, self.LINE_COLOR, 1)


class Stats(Reloadable):
    """Draw statistics on the screen and show interesting stuff."""

    serialize_vars = ()

    Y = 0

    def __init__(self, state=None, ship=None):

        self.X = Diagram(0, 900, "X", value=-1)
        self.T = Diagram(0, 720, "T/   ")
        self.T.setValueLabelFontSize(self.T.TEXT_SIZE/1.5)
        self.T.setValueLabelY(self.T.Y-self.T.valueLabelFontSize)
        self.V = Diagram(0, 720, "   V", (255, 0, 255))
        self.V.setValueLabelFontSize(self.V.TEXT_SIZE/1.5)
        self.V.setValueLabelY(self.V.Y+self.V.valueLabelFontSize)
        self.p_enabled = False
        self.P = Diagram(0, 540, "P", (0, 255, 0))
        self.d_enabled = False
        self.D = Diagram(0, 360, "D", (0, 255, 0))
        self.i_enabled = False
        self.I = Diagram(0, 180, "I", (0, 255, 0))

        self.ship = ship

        super().__init__(state)

    def draw(self):

        self.V.draw()
        self.T.draw()
        self.X.draw()
        if self.p_enabled:
            self.P.draw()
        if self.d_enabled:
            self.D.draw()
        if self.i_enabled:
            self.I.draw()

    def resize(self, screen_height, screen_width):
        self.X.resize(screen_height, screen_width)
        self.T.resize(screen_height, screen_width)
        self.P.resize(screen_height, screen_width)
        self.V.resize(screen_height, screen_width)
        self.D.resize(screen_height, screen_width)
        self.I.resize(screen_height, screen_width)

    def tick(self, ct, p, d, i):

        if ct % 2 == 0:
            self.X.add_point(self.ship.x)
            self.T.add_point(self.ship.thrust + self.ship.control)
            self.V.add_point(self.ship.speed*100)
            if p is not None:
                self.p_enabled = True
                self.P.add_point(p)
            if d is not None:
                self.d_enabled = True
                self.D.add_point(d)
            if i is not None:
                self.i_enabled = True
                self.I.add_point(i)




