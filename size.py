INIT_SCREEN_HEIGHT = 1280
INIT_SCREEN_WIDTH = 1800

"""
  This function 
"""
def real_width(size: int, curr_width: int) -> int:
    return real_size(size, curr_width, INIT_SCREEN_WIDTH)

def real_height(size: int, curr_height: int) -> int:
    return real_size(size, curr_height, INIT_SCREEN_HEIGHT)

def real_size(to_size: int, curr: int, start: int) -> int:
  
    return int(to_size * (curr / start))
